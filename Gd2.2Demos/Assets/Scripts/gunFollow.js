﻿#pragma strict

 var mouse_pos : Vector3; 
 var object_pos : Vector3;
 var angle : float;
 var mouse:Vector2;
 var coneRadius:float=10;
 private var target :GameObject;
 function Start()
 {

 }
 function Update ()
 {
 	if(target == null || !target.activeSelf)
 	{
 		try{
 		target = GameObject.FindGameObjectWithTag("Weapon");
 		}catch(e){
 			Debug.Log(e.ToString);
 		}
 	}
 	if(target != null)
 	{
     mouse_pos = Input.mousePosition;
     mouse_pos.z = 5.23; //The distance between the camera and object
     object_pos = Camera.main.WorldToScreenPoint(target.transform.position);
     mouse_pos.x = mouse_pos.x - object_pos.x;
     mouse_pos.y = mouse_pos.y - object_pos.y;
     angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;
       mouse = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
     
         if(mouse.x < Screen.width / 2)
         {
            transform.localScale = Vector3(1 , 1, transform.localScale.z);
            target.transform.localScale = Vector3(-1 , -1, transform.localScale.z);
            target.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
           // Debug.Log("Mouse on left screen");
         }
     
         if(mouse.x > Screen.width / 2)
         {
             transform.localScale = Vector3(-1 , 1, transform.localScale.z);
             target.transform.localScale = Vector3(1 , 1, transform.localScale.z);
             target.transform.rotation = Quaternion.AngleAxis(angle, Vector3.back);
            // Debug.Log("Mouse on right screen");
         }
	}
 }