﻿#pragma strict

public var canMove:boolean=true;
private var animator: Animator;
var walk:boolean=false;
var check : boolean = false;
var sightRange:float =0.5f;
var Layer:LayerMask;
var shield : GameObject;
//Weapons
var weapon : GameObject[];

//EnemyScript
var ene:Enemy;
var rigi:Rigidbody2D;
var gun : Gun;

//Shield
var shieldTime : float;
var coolDown : float;
var shieldOn : boolean;
var ItsON : boolean = false;

var movex:float;
var movey:float;

public var speed : float = 3.0f;

var manager : GameManager;

function Start()
{
	animator = GetComponent(Animator);
	ene = FindObjectOfType(Enemy);
	Layer = LayerMask.GetMask("wall");
	rigi = GetComponent(Rigidbody2D);
	gun = FindObjectOfType(Gun);
	manager = FindObjectOfType(GameManager);
}

function Update () 
{   
	//var move = Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
	//transform.position += move *speed *Time.deltaTime;
	//var move2 = (Input.GetAxis("Horizontal")||Input.GetAxis("Vertical"));
   // animator.SetFloat("speed", move2);
  	if(manager.playerHealth<=0)
  	{
  		manager.playerHealth = manager.playerMaxHealth;
  		Application.LoadLevel(4);
  	}
	if(!canMove)
	{
		walk = false;
		return;
	}

	if(walk)
	{
		animator.SetBool("walk",true);
	}
	else
	{
		animator.SetBool("walk",false);
	}

	walk = false;

	// Move the player based on WASD key inputs

    if(Input.GetKey(KeyCode.LeftShift))
    {
    	speed=6;
    }
    else
    {speed=4;}

    if(Input.GetKeyDown(KeyCode.Q))
    {
    	
    }
    if(Input.GetAxis("Horizontal")||Input.GetAxis("Vertical"))
	{
		walk=true;
	}

	if(Input.GetMouseButton(1))
	{
		ShieldUp();
		shieldTime -= Time.deltaTime;
	}
	else
	{
		shield.SetActive(false);
		ItsON = false;
	}
	coolDown -= Time.deltaTime;
}

function FixedUpdate()
{
	movex = Input.GetAxis("Horizontal");
	movey = Input.GetAxis("Vertical");
	rigi.velocity = new Vector2(movex*speed, movey*speed);
}

function changeWeapon()
{
	if(weapon[0]!=null && weapon[1]!= null)
	{
		var temp : GameObject;

		if(weapon[0].activeSelf)
		{
			weapon[0].SetActive(false);
			temp = weapon[0];
			weapon[0] = weapon[1];
			weapon[1] = temp;
			weapon[0].SetActive(true);
			check = true;
		}
	}
}

function ShieldUp()
{
	if(!shieldOn && coolDown < 0)
	{
		shieldTime = 3;
		shieldOn = true;
	}
	if(shieldTime > 0 && coolDown <= 0)
	{
		shield.SetActive(true);
		ItsON = true;
	}
	if(shieldTime <= 0 && shieldOn)
	{
		coolDown = 5;
		ItsON = false;
		shield.SetActive(false);
		shieldOn = false;
	}
	if(coolDown < 0)
	{
		
	}
}
