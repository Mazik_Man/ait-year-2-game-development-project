﻿#pragma strict

public var enemy:GameObject;

public var spawnTime:float;

public var spawnPoint: GameObject[];

var timeCount:float = 10;

function Update()
{
	SpawnEnemies();
}

function SpawnEnemies()
{
	
	timeCount -= Time.deltaTime;

	if(timeCount <= 0)
	{
		var randomSpawnPoint : int = Random.Range(0, spawnPoint.length);
		Instantiate(enemy, spawnPoint[randomSpawnPoint].transform.position, Quaternion.identity);
		timeCount = parseInt(Random.Range(5,15));
	}
}