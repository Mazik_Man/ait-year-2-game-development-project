﻿#pragma strict

public static var Score:int = 0;
public static var highScore:int = 0;
public static var instance:GameManager = null;

//player stats
public var playerHealth: float;
public var playerMaxHealth: float;
public var playerStr: float = 0;

//LevelSystem:
public var currLvl:int;
public var currExp:float;
private var maxLvl:int = 10;
public var maxExp:float = 1000;

var gun : Gun;

var gun1 : boolean = false;
var gun2 : boolean = false;
var gun3 : boolean = false;

var player : PlayerControlller;

function Awake () 
{
	if(instance == null)
		instance = this;
	else if(instance != this)
		Destroy(gameObject);

	gun = FindObjectOfType(Gun);
	player = FindObjectOfType(PlayerControlller);
	ChangeWeapon();
	DontDestroyOnLoad(gameObject);
}

function Start()
{
	playerHealth = playerMaxHealth;
}

function Update () 
{
	if(Score>=highScore)
	{
		highScore =Score;
	}

	ExpierienceSystem();
}
function OnLevelWasLoaded()
{
	player = FindObjectOfType(PlayerControlller);
	ChangeWeapon();
}
function ExpierienceSystem()
{
	if(currExp >= maxExp)
	{
		currLvl++;
		playerStr ++;
		gun.change=true;
		maxExp = Mathf.CeilToInt(maxExp * 2.2);
		currExp = 0;
	}
	if(currLvl >= maxLvl)
	{
		currLvl = maxLvl;
	}
}

function ChangeWeapon()
{
	if(gun1)
	{
		player.weapon[1].SetActive(false);
		player.weapon[2].SetActive(false);
		player.weapon[0].SetActive(true);
	}

	if(gun2)
	{
		player.weapon[1].SetActive(true);
		player.weapon[2].SetActive(false);
		player.weapon[0].SetActive(false);
	}

	if(gun3)
	{
		player.weapon[1].SetActive(false);
		player.weapon[2].SetActive(true);
		player.weapon[0].SetActive(false);
	}

}