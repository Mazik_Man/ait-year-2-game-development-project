﻿#pragma strict

var manager : GameManager;
var anim : Animator;
var timeToExplode : float;

function Start () {
	manager = FindObjectOfType(GameManager);
	anim = GetComponent(Animator);
	timeToExplode = 0.8;
}

function Update () 
{
	timeToExplode -= Time.deltaTime;

	if(timeToExplode <= 0)
	{
		anim.SetBool("active", true);
		if(anim.GetCurrentAnimatorStateInfo(0).IsName("trapExplode"))
		{
			Destroy(gameObject);
		}
	}
}

function OnTriggerEnter2D(other:Collider2D)
{
	if(other.gameObject.tag == "Player")
	{
		if(manager != null)
		{
			manager.playerHealth -=1;
		}
	}
}