﻿#pragma strict

var bulletSpeed:float;
function Start () {
	gameObject.GetComponent(Rigidbody2D).velocity = transform.up * bulletSpeed;
}

function Update () {

}
function OnTriggerEnter2D(other:Collider2D)
{
	if(other.transform.gameObject.tag=="Player")
	{
		Destroy(gameObject);
	}
	if(other.transform.gameObject.tag=="Wall")
	{
		Destroy(gameObject);
	}
}
function OnCollisionEnter2D(other:Collision2D)
{
	if(other.transform.gameObject.tag=="Player")
	{
		Destroy(gameObject);
	}
	if(other.transform.gameObject.tag=="Wall")
	{
		Destroy(gameObject);
	}
	if(other.transform.gameObject.tag=="armor")
	{
		Destroy(gameObject);
	}
}
