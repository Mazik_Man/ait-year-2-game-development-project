﻿#pragma strict

var player:GameObject;

public var speed : float = 3.0f;
public var enemyHealth:float = 3.0f;
var dis:float = 5.0f;
public var enemyDam:float;
public var expOrb:GameObject;
public var maxOrb:int = 5;
private var  bMovement : PlayerControlller;
public var gun : Gun;
private var attackTime:float = 0.0f;
var distance:float;
var decision : int =0;
var isTurning:boolean;
var waitingTime:float;
var deadObj : GameObject;
var enemySpawner : EnemySpawn;
private var rangeToClose:Vector3;
private var rangeToClose1:Vector3;
var anim :Animator;
var gunAttack:Transform;
var fireRate : float;
var fireRateTime:float;
var eneBullet : GameObject;
var weapon:GameObject;
var walkingSpeed:float;
var rayUP:GameObject[];
var rayDO:GameObject[];
var rayLE:GameObject[];
var rayRI:GameObject[];

enum enemyStates
{
	CHASE,
	PATROL,
	DEAD,
	STOP
}

enum enemyAttack
{
	ATTACK,
	GUNATTACK,
	SHOTGUNATTACK
}

private var howMuchWeMoveInAFrame:float;
//walking
var right : boolean;
var left : boolean;
var up: boolean;
var down: boolean;
//
public var state: enemyStates;
public var state2: enemyAttack;
public var curAttackState: enemyAttack;
var sightRange:float =0.5f;
private var moveVec3:Vector3;
var rigi:Rigidbody2D;
var knockbackPatrol:boolean;
private var Layer:LayerMask;
public var powerUp : GameObject[];

function Start () 
{
	player = GameObject.FindGameObjectWithTag("Player");
	bMovement = FindObjectOfType(PlayerControlller);
	gun = FindObjectOfType(Gun);
	enemySpawner = FindObjectOfType(EnemySpawn);
	Layer = LayerMask.GetMask("wall");
	anim = GetComponent(Animator);
	rigi = GetComponent(Rigidbody2D);
	var randomStart : int = Random.Range(1,5);
		switch(randomStart)
		{
			case 1:
				down = true;
				break;
			case 2:
				left = true;
				break;
			case 3:
				up = true;
				break;
			case 4:
				right = true;
				break;
		}
}
function Update()
{
	if(state == state.DEAD)
	{
		Dead();
		spawnPowerUp();
	}
	if(enemyHealth <= 0)
	{
		state = state.DEAD;
	}
}
function FixedUpdate () 
{
	if(state == state.PATROL)
	{
		FollowPoints();
		walk();
	}
	if(state == state.CHASE)
	{
		followPlayer();
	}
	if(state == state.STOP)
	{
		FollowPoints();
		walk();
	}
	if(state2 == state2.ATTACK)
	{
		Attack();
	}


	rangeToClose1 = player.transform.position - transform.position;
	distance = rangeToClose1.magnitude;
	attackTime-=Time.deltaTime;
	if(distance > dis && enemyHealth>0)
	{
		state = state.PATROL;
	}
	if(distance<=0.8 && enemyHealth>0 || bMovement.ItsON == true)
	{
		state=state.STOP;
	}

	if(curAttackState == curAttackState.ATTACK && distance<=2.2 && enemyHealth>0 && state != state.STOP)
	{
		state2=state2.ATTACK;
	}
	else
	{
		anim.SetBool("Attack",false);
	}

	if(curAttackState == curAttackState.GUNATTACK && distance<=5 && enemyHealth>0 && state != state.STOP)
	{
		GunAttack();
	}

	if(curAttackState == curAttackState.SHOTGUNATTACK && distance<=5 && enemyHealth>0 && state != state.STOP)
	{
		ShotGunAttack();
	}

	if(distance < dis && enemyHealth>0 && distance > 0.8)
	{
		state = state.CHASE;
	}

	if(gun == null)
	{
		gun = FindObjectOfType(Gun);
	}

	rangeToClose = (player.transform.position - transform.position).normalized;
	howMuchWeMoveInAFrame = (speed+2);
	moveVec3 = rangeToClose * howMuchWeMoveInAFrame;
}

function Attack()
{
	anim.SetBool("Attack",true);
}
function Dead()
{
	for(var i:int =0; i <= maxOrb; i++)
		{
			Instantiate(expOrb, new Vector3(transform.position.x + Random.Range(0.0, 1.0), transform.position.y + Random.Range(0.0, 1.0), 0 ), Quaternion.identity);
		}
		Instantiate(deadObj, transform.position, Quaternion.identity);
		Destroy(gameObject);
		GameManager.instance.Score += 5;
}
function spawnPowerUp()
{
	var powerChance : int = 5;
	var powerPrecentage : int = Random.Range(0,100);

	if(powerPrecentage < powerChance)
	{
		var randPowerUp : int = Random.Range(0, powerUp.length);
		Instantiate(powerUp[randPowerUp],transform.position,Quaternion.identity);
	}
}
function GunAttack()
{
	var guns:Vector3;
	guns.x = weapon.transform.position.x - player.transform.position.x;
	guns.y = weapon.transform.position.y - player.transform.position.y;

	var	angle2 = Mathf.Atan2(guns.y, guns.x) * Mathf.Rad2Deg;


	if((transform.position.x - player.transform.position.x) < 0)
	{
		transform.localScale = Vector3(1 , 1, 1);
        weapon.transform.localScale = Vector3(1 , -1, transform.localScale.z);
        weapon.transform.rotation = Quaternion.AngleAxis(angle2, Vector3.forward);
	}
	else
	{
		transform.localScale = Vector3(-1 , 1, 1);
        weapon.transform.localScale = Vector3(-1 , 1, transform.localScale.z);
        weapon.transform.rotation = Quaternion.AngleAxis(angle2, Vector3.back);
	}
	fireRate -= Time.deltaTime;
    var shootDirection:Vector3 = (player.transform.position - gunAttack.position).normalized;
    if(fireRate <= 0)
    {
        var angle :  float = Mathf.Atan2 (shootDirection.y, shootDirection.x) * Mathf.Rad2Deg -90;
        var spread : float = Random.Range(-1.5, 1.5);
		var bulletRotation : Quaternion =  Quaternion.Euler(new Vector3(0, 0, angle + spread));
		var projectile:GameObject = GameObject.Instantiate( eneBullet, gunAttack.position, bulletRotation);
		fireRate = fireRateTime;
	}
}
function ShotGunAttack()
{
	var guns:Vector3;
	guns.x = weapon.transform.position.x - player.transform.position.x;
	guns.y = weapon.transform.position.y - player.transform.position.y;

	var	angle2 = Mathf.Atan2(guns.y, guns.x) * Mathf.Rad2Deg;


	if((transform.position.x - player.transform.position.x) < 0)
	{
		transform.localScale = Vector3(1.5 , 1.5, 1);
        weapon.transform.localScale = Vector3(1 , -1, transform.localScale.z);
        weapon.transform.rotation = Quaternion.AngleAxis(angle2, Vector3.forward);
	}
	else
	{
		transform.localScale = Vector3(-1.5 , 1.5, 1);
        weapon.transform.localScale = Vector3(-1 , 1, transform.localScale.z);
        weapon.transform.rotation = Quaternion.AngleAxis(angle2, Vector3.back);
	}
	fireRate -= Time.deltaTime;
    var shootDirection:Vector3 = (player.transform.position - gunAttack.position).normalized;
    if(fireRate <= 0)
    {
    	for(var i :int = 0; i < 4; i ++)
    	{
	        var angle :  float = Mathf.Atan2 (shootDirection.y, shootDirection.x) * Mathf.Rad2Deg -90;
	        var spread : float = Random.Range(-10, 10);
			var bulletRotation : Quaternion =  Quaternion.Euler(new Vector3(0, 0, angle + spread));
			var projectile:GameObject = GameObject.Instantiate( eneBullet, gunAttack.position, bulletRotation);
		}
		fireRate = fireRateTime;
	}
}
function followPlayer()
{
	var hitUP1 : RaycastHit2D = Physics2D.Raycast(rayUP[0].transform.position, transform.up, sightRange,Layer);
	var hitUP2 : RaycastHit2D = Physics2D.Raycast(rayUP[1].transform.position, transform.up, sightRange,Layer);
	var hitUP3 : RaycastHit2D = Physics2D.Raycast(rayUP[2].transform.position, transform.up, sightRange,Layer);
	var hitDO1 : RaycastHit2D = Physics2D.Raycast(rayDO[0].transform.position, -transform.up, sightRange,Layer);
	var hitDO2 : RaycastHit2D = Physics2D.Raycast(rayDO[1].transform.position, -transform.up, sightRange,Layer);
	var hitDO3 : RaycastHit2D = Physics2D.Raycast(rayDO[2].transform.position, -transform.up, sightRange,Layer);
	var hitLE1 : RaycastHit2D = Physics2D.Raycast(rayLE[0].transform.position, -transform.right, sightRange,Layer);
	var hitLE2 : RaycastHit2D = Physics2D.Raycast(rayLE[1].transform.position, -transform.right, sightRange,Layer);
	var hitLE3 : RaycastHit2D = Physics2D.Raycast(rayLE[2].transform.position, -transform.right, sightRange,Layer);
	var hitRI1 : RaycastHit2D = Physics2D.Raycast(rayRI[0].transform.position, transform.right, sightRange,Layer);
	var hitRI2 : RaycastHit2D = Physics2D.Raycast(rayRI[1].transform.position, transform.right, sightRange,Layer);
	var hitRI3 : RaycastHit2D = Physics2D.Raycast(rayRI[2].transform.position, transform.right, sightRange,Layer);

	if(hitUP1 || hitUP2 || hitUP3)
	{
		rigi.velocity = new Vector2(moveVec3.x,-speed);
	}
	else if(hitDO1 || hitDO2 || hitDO3)
	{
		rigi.velocity = new Vector2(moveVec3.x,speed);
	}
	else if(hitLE1 || hitLE2 || hitLE3)
	{
		rigi.velocity = new Vector2(speed, speed);
	}
	else if(hitRI1 || hitRI2 || hitRI3)
	{
		rigi.velocity = new Vector2(-speed, speed);
	}
	else
	{
		rigi.velocity = new Vector2(moveVec3.x,moveVec3.y);
	}
}
function FollowPoints()
{
	var hitUP1 : RaycastHit2D = Physics2D.Raycast(rayUP[0].transform.position, transform.up, sightRange,Layer);
	var hitUP2 : RaycastHit2D = Physics2D.Raycast(rayUP[1].transform.position, transform.up, sightRange,Layer);
	var hitUP3 : RaycastHit2D = Physics2D.Raycast(rayUP[2].transform.position, transform.up, sightRange,Layer);
	var hitDO1 : RaycastHit2D = Physics2D.Raycast(rayDO[0].transform.position, -transform.up, sightRange,Layer);
	var hitDO2 : RaycastHit2D = Physics2D.Raycast(rayDO[1].transform.position, -transform.up, sightRange,Layer);
	var hitDO3 : RaycastHit2D = Physics2D.Raycast(rayDO[2].transform.position, -transform.up, sightRange,Layer);
	var hitLE1 : RaycastHit2D = Physics2D.Raycast(rayLE[0].transform.position, -transform.right, sightRange,Layer);
	var hitLE2 : RaycastHit2D = Physics2D.Raycast(rayLE[1].transform.position, -transform.right, sightRange,Layer);
	var hitLE3 : RaycastHit2D = Physics2D.Raycast(rayLE[2].transform.position, -transform.right, sightRange,Layer);
	var hitRI1 : RaycastHit2D = Physics2D.Raycast(rayRI[0].transform.position, transform.right, sightRange,Layer);
	var hitRI2 : RaycastHit2D = Physics2D.Raycast(rayRI[1].transform.position, transform.right, sightRange,Layer);
	var hitRI3 : RaycastHit2D = Physics2D.Raycast(rayRI[2].transform.position, transform.right, sightRange,Layer);
	if(knockbackPatrol)
	{
		var howMuchWeMoveInAFrame2 = (speed+5);
		var knock = rangeToClose * howMuchWeMoveInAFrame2;
		rigi.velocity = new Vector2(knock.x,knock.y);
		knockbackPatrol=false;
	}
	else
	{
		if(hitUP1 || hitUP2 || hitUP3)
		{
			var randomUP : int = Random.Range(1,4);
			switch(randomUP)
			{
				case 1:
					down = true;
					up=false;
					left=false;
					right=false;
					break;
				case 2:
					down = true;
					up=false;
					left=false;
					right=true;
					break;
				case 3:
					down = true;
					up=false;
					left=true;
					right=false;
					break;
			}
		}
		if(hitDO1 || hitDO2 || hitDO3)
		{
			var randomDOWN : int = Random.Range(1,4);
			switch(randomDOWN)
			{
				case 1:
					down = false;
					up=true;
					left=false;
					right=false;
					break;
				case 2:
					down = false;
					up=true;
					left=false;
					right=true;
					break;
				case 3:
					down = false;
					up=true;
					left=true;
					right=false;
					break;
			}
		}
		if(hitLE1 || hitLE2 || hitLE3)
		{
			var randomLEFT : int = Random.Range(1,4);
			switch(randomLEFT)
			{
				case 1:
					down = true;
					up=false;
					left=false;
					right=true;
					break;
				case 2:
					down = false;
					up=false;
					left=false;
					right=true;
					break;
				case 3:
					down = false;
					up=true;
					left=false;
					right=true;
					break;
			}
		}
		if(hitRI1 || hitRI2 || hitRI3)
		{
			var randomRIGHT : int = Random.Range(1,4);
			switch(randomRIGHT)
			{
				case 1:
					down = true;
					up=false;
					left=true;
					right=false;
					break;
				case 2:
					down = false;
					up=false;
					left=true;
					right=false;
					break;
				case 3:
					down = false;
					up=true;
					left=true;
					right=false;
					break;
			}
		}
	}
}
function walk()
{
	if(down)
	{
		rigi.velocity.y = -walkingSpeed;
	}
	if(up)
	{
		rigi.velocity.y = walkingSpeed;
	}
	if(right)
	{
		rigi.velocity.x = walkingSpeed;
	}
	if(left)
	{
		rigi.velocity.x = -walkingSpeed;
	}
}
function OnCollisionEnter2D(other:Collision2D)
{
	if(other.transform.gameObject.tag=="bullet")
	{
		//Debug.Log("Hit");
		if(gun != null)
		{
			enemyHealth -= gun.dmg;
			knockbackPatrol = true;
		}

		//Debug.Log("Health = " + enemyHealth);

	}
}

