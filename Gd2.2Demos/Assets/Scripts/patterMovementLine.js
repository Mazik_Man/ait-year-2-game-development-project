﻿/*#pragma strict

public class ControlDataW
{
	public function ControlDataW(pos : Transform)
	{
		point = pos;
	}

	public var point: Transform;
}

var pattern : ControlDataW[];

@HideInInspector
var patterPoint:int;

public var speed : float = 1.0f;

function Start()
{
	var p1 : ControlDataW = ControlDataW(new Vector3(-5, 4, 0));

	var p2 : ControlDataW = ControlDataW(new Vector3(-5, -3, 0));

	var p3 : ControlDataW = ControlDataW(new Vector3(7, -3, 0));

	var p4 : ControlDataW = ControlDataW(new Vector3(7, 4, 0));

	pattern = [p1, p2, p3, p4];

}

function Update()
{
	var cd : ControlDataW = pattern[patterPoint];

	var rangeToClose = cd.point.position - transform.position;

	var distance = rangeToClose.magnitude;

	var RangeToClosenorm = rangeToClose.normalized;
	Debug.DrawRay(transform.position,rangeToClose,Color.cyan);

	transform.position += RangeToClosenorm * speed * Time.deltaTime;

	Debug.DrawRay(transform.position, RangeToClosenorm, Color.magenta);

	if(distance <= 0.1)
	{
		patterPoint++;
	}

	if(patterPoint >= pattern.length)
	{
		patterPoint =0;
	}
}*/