﻿#pragma strict
public var theText:TextAsset;
public var door:GameObject;
public var startLine:int;
public var endLine:int;

public var theTextBox:TextBoxManager;

public var destroyWhenActivated:boolean;

public var goToNextLevel:boolean;
public var doorDestroy:boolean;

function Start () 
{
	theTextBox = FindObjectOfType(TextBoxManager);
}

function OnTriggerEnter2D(other:Collider2D)
{
	if(other.transform.gameObject.tag == "Player")
	{
		theTextBox.ReloadScript(theText);
		theTextBox.currentLine = startLine;
		theTextBox.endAtLine = endLine;
		theTextBox.EnableTextBox();
		
		if(destroyWhenActivated)
		{
			Destroy(gameObject);
		}
		if(goToNextLevel)
		{
			 theTextBox.goToNextLevel = true;		
		}
		if(doorDestroy)
		{
			theTextBox.DoorDestroy(door);
		}
	}
}