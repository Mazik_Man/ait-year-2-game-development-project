﻿#pragma strict

public var player:GameObject;       

private var offset:Vector3; 
        
function Start () 
{
    //player = GameObject.Find("Player");

}
function Update()
{
	if(player==null)
	{
		player = GameObject.FindGameObjectWithTag("Player");
		offset = transform.position - player.transform.position;
	}
}  
function LateUpdate () 
{
	if(player!=null)
	{
		transform.position = player.transform.position + offset;
	}
}
