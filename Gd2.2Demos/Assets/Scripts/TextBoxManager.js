﻿#pragma strict
import UnityEngine.UI;

public var textBox:GameObject;
public var doors:GameObject;

public var theText:Text;

public var currentLine:int;
public var endAtLine:int;

public var textfile:TextAsset;
public var textLines:String[];

public var player:PlayerControlller;

public var isActive:boolean;
public var stopMovingPlayer:boolean =true;
public var goToNextLevel:boolean;
public var destroyDoors:boolean;

public var levelNum:int;
function Start () 
{	
	player = FindObjectOfType(PlayerControlller);
	if(textfile != null)
	{
		textLines = (textfile.text.Split("\n"[0]));
	}
	
	if(endAtLine == 0)
	{
		endAtLine = textLines.Length -1;
	}
	
	if(isActive)
	{
		EnableTextBox();
	}
	else
	{
		DisableTextBox();
	}
}

function Update () 
{

	if(!isActive)
	{
		return;
	}
	
	theText.text = textLines[currentLine];
	if(Input.GetKeyDown(KeyCode.Space))
	{
		currentLine++;
	}
	
	if(currentLine > endAtLine)
	{
		GoToNextLevelWhenTextIsFinished();
		DisableTextBox();
	}
}

function EnableTextBox()
{
	textBox.SetActive(true);
	isActive = true;
	Time.timeScale=0;
	if(stopMovingPlayer)
	{
		player.canMove = false;
	}
}

function DisableTextBox()
{
	Time.timeScale=1;
	textBox.SetActive(false);
	player.canMove = true;
	isActive=false;
	if(destroyDoors)
	{
		DoorDestroy(doors);
	}
}

function ReloadScript(theText:TextAsset)
{
	if(theText!=null)
	{
		textLines = new String[1];
		textLines = (theText.text.Split("\n"[0]));
	}
}
function DoorDestroy(door:GameObject)
{
	this.doors=door;
	Destroy(doors);
}
function GoToNextLevelWhenTextIsFinished()
{
	if(goToNextLevel)
	{
		Application.LoadLevel(levelNum);
	}
}