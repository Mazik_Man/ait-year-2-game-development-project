﻿#pragma strict

var healthPowerUp : boolean;
var strPowerUp : boolean;
var manager : GameManager;
var player : GameObject;
var distance : float;

function Start()
{
	manager = FindObjectOfType(GameManager);
	player = GameObject.FindGameObjectWithTag("Player");
}

function Update()
{
	PowerMeUp();
}

function PowerMeUp()
{
	var distanceBetween:Vector3 = transform.position - player.transform.position;
	distance = distanceBetween.magnitude;

	if(healthPowerUp)
	{
		if(distance <= 0.5)
		{
			if(Input.GetKeyDown(KeyCode.E))
			{
				manager.playerMaxHealth += 5;
				manager.playerHealth += 5;
				Destroy(this.gameObject);
			}
		}
	}
	if(strPowerUp)
	{
		if(distance <= 0.5)
		{
			if(Input.GetKeyDown(KeyCode.E))
			{
				manager.playerStr += 1;
				Destroy(this.gameObject);
			}
		}
	}
}