﻿#pragma strict
import UnityEngine.UI;

public var txt:Text;
public var healthTxt:Text;
public var highScoreTxt:Text;
public var ammoTxt:Text;
private var ammoManager:Gun;
public var reloadinText:Text;
private var manager : GameManager;
private var pMovement : PlayerControlller;
public var helth:Image;
public var expText : Text;
public var expImg : Image;
public var lvlText : Text;

private var boss : BossAlien;
public var bossHealth : Image;
public var bossHealthText : Text;

function Start()
{
	ammoManager = FindObjectOfType(Gun);
	manager = FindObjectOfType(GameManager);
	boss = FindObjectOfType(BossAlien);
	pMovement = FindObjectOfType(PlayerControlller);
}
function Update()
{
	txt.text = "" + GameManager.instance.Score;
	healthTxt.text = "" + manager.playerHealth;
	expText.text = "" + manager.currExp + " / " + manager.maxExp;
	lvlText.text = "" + manager.currLvl;
	helth.fillAmount = manager.playerHealth / manager.playerMaxHealth;
	expImg.fillAmount = manager.currExp / manager.maxExp;

	if(ammoManager!=null && pMovement.check)
	{
		ammoTxt.text = "Ammo: " + ammoManager.ammoSize + " / " + ammoManager.maxAmmo;
		if(ammoManager.reloading)
		{
			reloadinText.text = "Reloading...";
		}
		else if(ammoManager.ammoSize <= 0)
		{
			reloadinText.text = "Reload!";
		}
		else
		{
			reloadinText.text = " ";
		}
	}
	else
	{
		ammoManager = FindObjectOfType(Gun);
		pMovement.check = true;
	}

	if(boss != null)
	{
		bossHealthText.text = "" + Mathf.Round(boss.bossHealth) + " / " + boss.bossMaxHealth;
		bossHealth.fillAmount = boss.bossHealth / boss.bossMaxHealth;
	}
}