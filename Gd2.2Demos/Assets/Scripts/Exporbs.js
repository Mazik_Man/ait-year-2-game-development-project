﻿#pragma strict

private var expToGive:int = 5;
private var player:GameObject;
var  manager : GameManager;
public var speed:float= 5.0f;
var dis:float = 1.5f;

function Start () 
{
	player = GameObject.FindGameObjectWithTag("Player");
	manager = FindObjectOfType(GameManager);
}

function Update () 
{
	var rangeToClose:Vector3 = player.transform.position - transform.position;

	var distance:float = rangeToClose.magnitude;

	var normalizedRangeToClose = rangeToClose.normalized;
	var howMuchWeMoveInAFrame = speed * Time.deltaTime;


	if(distance < dis)
	{
		var moveVec = normalizedRangeToClose * howMuchWeMoveInAFrame;
		transform.position += moveVec;
	}
	else
	{
		
	}
}

function OnCollisionEnter2D(col:Collision2D)
{
	if(col.gameObject.tag == "Player")
	{
		Debug.Log("collides");
		if(manager!=null)
		{
			manager.currExp += expToGive;
			Destroy(gameObject);
		}
	}
}
