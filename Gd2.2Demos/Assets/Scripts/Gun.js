﻿#pragma strict

var bullet:GameObject;
var distance:float = 0;
public var dmg: float;
public var gunDmg: float =1;
@SerializeField
private var fireRate:float;
public var fireRateTime:float;

public var ammoSize:int;
public var maxAmmoSize:int;
public var maxAmmo:int;

@SerializeField
private var reloadTime:float;
public var timeToReload:float;

public var reloading:boolean = false;

public var bulletNumber:int;

var manager : GameManager;

var change : boolean = true;

var minSpread : float = -2.5;

var maxSpread : float = 2.5;

enum gunState{
	PISTOL,
	SHOTGUN,
	MACHINEGUN,
	RELOAD
}

var state : gunState;
var curState : gunState;

function Awake () {
	curState = state;
	manager = FindObjectOfType(GameManager);
}

function Update () 
{
	if(state==gunState.SHOTGUN)
	{
		shotGun();
		fireRate -= Time.deltaTime;
	}
	if(state==gunState.PISTOL)
	{
		if(Input.GetMouseButtonDown(0) && ammoSize > 0)
		{
			Gun();
		}
		fireRate -= Time.deltaTime;
	}
	if(state==gunState.MACHINEGUN)
	{
		if(Input.GetMouseButton(0) && ammoSize > 0)
		{
			Gun();
		}
		fireRate -= Time.deltaTime;
	}
	if(state==state.RELOAD)
	{
		reload();
	}

	if(Input.GetKeyDown(KeyCode.R))
	{
		if(ammoSize < maxAmmoSize)
		{
			timeToReload = reloadTime;
			state = state.RELOAD;
			reloading = true;
		}
	}
	if(change)
	{
		dmg = gunDmg + manager.playerStr * 0.8;
		change=false;
	}

}

function Gun()
{
	var myPos:Vector3 = Camera.main.WorldToScreenPoint(transform.position);
    var direction:Vector3 = (Input.mousePosition - myPos).normalized;
    if(fireRate <= 0 && direction.magnitude > 0.2f)
    {
        var angle :  float = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg -90;
        var spread : float = Random.Range(minSpread, maxSpread);
		var bulletRotation : Quaternion =  Quaternion.Euler(new Vector3(0, 0, angle + spread));
		var projectile:GameObject = GameObject.Instantiate( bullet, transform.position, bulletRotation);

		ammoSize--;
		fireRate = fireRateTime;
	}
}
function shotGun()
{
	var myPos:Vector3 = Camera.main.WorldToScreenPoint(transform.position);
    var direction:Vector3 = (Input.mousePosition - myPos).normalized;
	if(Input.GetMouseButtonDown(0) && ammoSize > 0)
	{
	    if(fireRate <= 0 && direction.magnitude > 0.2f)
	    {
	    	for(var i:int = 0; i  < bulletNumber; i++)
	    	{
		        var angle :  float = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg -90;
		        var spread : float = Random.Range(-10, 10);
				var bulletRotation : Quaternion =  Quaternion.Euler(new Vector3(0, 0, angle + spread));
				var projectile:GameObject = GameObject.Instantiate( bullet, transform.position, bulletRotation);

				ammoSize--;
				fireRate = fireRateTime;
			}
		}
	}
}
function reload()
{
	timeToReload -= Time.deltaTime;

	if(reloading == true)
	{

					
				var ammo = maxAmmoSize - ammoSize;
				if(maxAmmo >= ammo)
				{
					ammoSize = maxAmmoSize;
					maxAmmo -= ammo;


				}
				if(maxAmmo < ammo)
				{
					ammoSize += maxAmmo;
					maxAmmo -= maxAmmo;
				}

		if(timeToReload <= 0)
		{
			reloading = false;
			state = curState;
		}
	}
}