﻿#pragma strict
var bulletSpeed:float;
function Start () {
	gameObject.GetComponent(Rigidbody2D).velocity = transform.up * bulletSpeed;
}

function Update () {

}
function OnCollisionEnter2D(other:Collision2D)
{
	if(other.transform.gameObject.tag=="Enemy")
	{
		Destroy(gameObject);
	}
	if(other.transform.gameObject.tag=="Wall")
	{
		Destroy(gameObject);
	}
	if(other.transform.gameObject.tag=="Boss")
	{
		Destroy(gameObject);
	}
}
