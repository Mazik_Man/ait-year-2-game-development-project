﻿#pragma strict

enum bossBehaviour
{
	NORMAL,
	AGGRESSIVE
}
var behaviour : bossBehaviour;

enum states
{
	NOTHING,
	SHOOT,
	TRAP
}

//States
var state : states;

//Boss Stats
var bossHealth : float;

var bossMaxHealth : float = 150;

var speed : float = 2;

var fasterSpeed : float = 3;

//GameObjects and scripts
var bullet : GameObject;

var bossTrap : GameObject;

var player : GameObject;

var gun : Gun;

//Random Walking
var waitTime : float;

var wait : boolean = true;

var walk : int;

var walkToPlayer : boolean = false;

var walkToPlayerFast : boolean = false;

//shooting
var angle : float;
var bulletRotation : Quaternion;
var projectile : GameObject;
var waitTillAttack : boolean;
var timeTillAttack : float;

function Start ()
{
	bossHealth = bossMaxHealth;	
	player = GameObject.FindGameObjectWithTag("Player");
	gun = FindObjectOfType(Gun);
	walkToPlayer = true;
	timeTillAttack =parseInt(Random.Range(2,5));
}

function Update ()
{
	if(bossHealth <= 0)
	{
		Destroy(gameObject);
	}

	if(bossHealth >= 150)
	{
		behaviour = behaviour.NORMAL;
	}
	else
	{
		behaviour = behaviour.AGGRESSIVE;
	}

	if(state == state.SHOOT)
	{
		shoot();
	}
	if(state == state.TRAP)
	{
		trap();
	}
	if(behaviour == behaviour.NORMAL)
	{
		wander();
		positionToWalk();
		normalBehaviour();
	}
	if(behaviour == behaviour.AGGRESSIVE)
	{
		wander();
		positionToWalk();
		aggressiveBehaviour();
	}

	timeTillAttack -= Time.deltaTime;
	waitTime-=Time.deltaTime;

}

function shoot()
{
	if(behaviour == behaviour.NORMAL)
	{
		var shooting : int = Random.Range(1,3);
		switch(shooting)
		{
			case 1:
				for(var i : int = 0; i < 10; i ++)
				{
					angle = (i*10) * Mathf.Rad2Deg;
					bulletRotation =  Quaternion.Euler(new Vector3(0, 0, angle));
					projectile = GameObject.Instantiate( bullet, transform.position, bulletRotation);
				}
				break;
			case 2:
				for(i = 0; i < 9; i ++)
				{
					angle = (i*30) * Mathf.Rad2Deg;
					bulletRotation =  Quaternion.Euler(new Vector3(0, 0, angle));
					projectile = GameObject.Instantiate( bullet, transform.position, bulletRotation);
				}
		}
	}
	else
	{
		shooting = Random.Range(1,4);
		switch(shooting)
		{
			case 1:
				waitTime = 2;
				walkToPlayerFast = false;
				walkToPlayer = false;
				for(i = 0; i < 2; i ++)
				{
					for(var j : int = 0; j < 10; j ++)
					{
						angle = (j*5 + i) * Mathf.Rad2Deg;
						bulletRotation =  Quaternion.Euler(new Vector3(0, 0, angle));
						projectile = GameObject.Instantiate( bullet, transform.position, bulletRotation);
						yield WaitForSeconds(0.01);
					}
				}
				break;
			case 2:
				for(i = 0; i < 9; i ++)
				{
					angle = (i*10) * Mathf.Rad2Deg;
					bulletRotation =  Quaternion.Euler(new Vector3(0, 0, angle));
					projectile = GameObject.Instantiate( bullet, transform.position, bulletRotation);
				}
				break;
			case 3:
				for(i = 0; i < 9; i ++)
				{
					angle = (i*30) * Mathf.Rad2Deg;
					bulletRotation =  Quaternion.Euler(new Vector3(0, 0, angle));
					projectile = GameObject.Instantiate( bullet, transform.position, bulletRotation);
				}
		}
	} 

		state = state.NOTHING;
}
function wander()
{
	if(wait)
	{
		waitTime =parseInt(Random.Range(2,5));
		walk = Random.Range(1,5);
		wait = false;
	}

	if(waitTime <= 0 )
	{
		switch(walk)
		{
			case 1:
				walkToPlayer = true;
				walkToPlayerFast = false;
				wait = true;
				break;
			case 2:
				walkToPlayerFast = true;
				walkToPlayer = false;
				wait = true;
				break;
			case 3:
				walkToPlayerFast = false;
				walkToPlayer = false;
				wait = true;
				break;
			case 4:
				walkToPlayerFast = false;
				walkToPlayer = true;
				wait = true;
				break;
		}
	}
}

function positionToWalk()
{
	if(walkToPlayer)
	{
		var goTowardsPlayer = (player.transform.position - transform.position).normalized;
		transform.position += goTowardsPlayer * speed * Time.deltaTime;
	}
	if(walkToPlayerFast)
	{
		goTowardsPlayer = (player.transform.position - transform.position).normalized;
		transform.position += goTowardsPlayer * fasterSpeed * Time.deltaTime;
	}
}

function trap()
{
	if(player != null)
	{
		if(behaviour == behaviour.NORMAL)
		{
			var trapSwitch : int = Random.Range(1,3);
			switch(trapSwitch)
			{
				case 1:
					Instantiate(bossTrap, player.transform.position, Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x, player.transform.position.y + 1.4, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x, player.transform.position.y - 1.4, 1), Quaternion.identity);
					break;
				case 2:
					Instantiate(bossTrap, player.transform.position, Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x + 1.4, player.transform.position.y, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x - 1.4, player.transform.position.y, 1), Quaternion.identity);
					break;
			}
		}
		else
		{
			trapSwitch = Random.Range(1,3);
			switch(trapSwitch)
			{
				case 1:
					Instantiate(bossTrap, player.transform.position, Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x, player.transform.position.y + 1.4, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x, player.transform.position.y - 1.4, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x + 1.4, player.transform.position.y, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x - 1.4, player.transform.position.y, 1), Quaternion.identity);
					break;
				case 2:
					Instantiate(bossTrap, player.transform.position, Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x, player.transform.position.y + 1.4, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x, player.transform.position.y - 1.4, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x + 1.4, player.transform.position.y, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x - 1.4, player.transform.position.y, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x, player.transform.position.y + 2.8, 1), Quaternion.identity);
					Instantiate(bossTrap, new Vector3(player.transform.position.x, player.transform.position.y - 2.8, 1), Quaternion.identity);
					break;
			}
		}

		state = state.NOTHING;
	}
}
function normalBehaviour()
{
	if(waitTillAttack)
	{
		timeTillAttack = parseInt(Random.Range(2,6));
		waitTillAttack = false;
	}

	if(timeTillAttack <= 0)
	{
		var ran : int = Random.Range(1, 3);

		switch(ran)
		{
			case 1:
				state = state.SHOOT;
				waitTillAttack = true;
				break;
			case 2:
				state = state.TRAP;
				waitTillAttack = true;
				break;
		}
	}
}
function aggressiveBehaviour()
{
	if(waitTillAttack)
	{
		timeTillAttack = parseInt(Random.Range(1,3));
		waitTillAttack = false;
	}

	if(timeTillAttack <= 0)
	{
		var ran : int = Random.Range(1, 3);

		switch(ran)
		{
			case 1:
				shoot();
				waitTillAttack = true;
				break;
			case 2:
				trap();
				waitTillAttack = true;
				break;
		}
	}
}
function OnCollisionEnter2D(other:Collision2D)
{
	if(other.transform.gameObject.tag=="bullet")
	{
		if(gun != null)
		{
			bossHealth -= gun.dmg;
		}
	}
}
