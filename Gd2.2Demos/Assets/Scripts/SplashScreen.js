﻿#pragma strict
import UnityEngine.UI;
import UnityEngine.SceneManagement;

var logo : Image;
public var loadlevel:String;

function Start () {
	finish();
}

function Update () {
	
}

function finish()
{
	logo.canvasRenderer.SetAlpha(0.0f);
	yield WaitForSeconds(2.5f);
	FadeIn();
	yield WaitForSeconds(2.5f);
	FadeOut();
	yield WaitForSeconds(2.5f);
	SceneManager.LoadScene(loadlevel);
}

function FadeIn()
{
	logo.CrossFadeAlpha(1.0f, 1.5f, false);
}

function FadeOut()
{
	logo.CrossFadeAlpha(0.0f, 1.5f, false);
}