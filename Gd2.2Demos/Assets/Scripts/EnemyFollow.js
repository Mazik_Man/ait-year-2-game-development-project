﻿#pragma strict

var player:Transform;
var speed:float;
var timeCheck:float = 10;
var timeCheck2:float = 10;
private var enemyHealth:float = 3.0f;
function Start ()
{
	
}

function Update () 
{
	if(enemyHealth <= 0)
	{
		Destroy(gameObject);
	}
	timeCheck -= Time.deltaTime;

	if(timeCheck <=0)
	{
		timeCheck2 -= Time.deltaTime;
		Evade();
		if(timeCheck2 <= 0)
		{
			timeCheck = 10;
			timeCheck2 = 10;
		}
	}
	else
	{
		Chase();
	}

}

function Chase()
{
	var speedDelta:float = speed * Time.deltaTime;


		if(player.position.x > transform.position.x )
		{
			transform.position.x +=speedDelta;
		}
		if(player.position.x < transform.position.x )
		{
			transform.position.x -=speedDelta;
		}
	


		if(player.position.y > transform.position.y )
		{
			transform.position.y +=speedDelta;
		}
		if(player.position.y < transform.position.y )
		{
			transform.position.y -=speedDelta;
		}
	
}

function Evade()
{
	var speedDelta:float = speed * Time.deltaTime;

	if(Mathf.Abs(transform.position.x - player.position.x) < 3)
	{
		if(player.position.x > transform.position.x )
		{
			transform.position.x -=speedDelta;
		}
		if(player.position.x < transform.position.x )
		{
			transform.position.x +=speedDelta;
		}
	}

	if(Mathf.Abs(transform.position.y - player.position.y) < 3)
	{
		if(player.position.y > transform.position.y )
		{
			transform.position.y -=speedDelta;
		}
		if(player.position.y < transform.position.y )
		{
			transform.position.y +=speedDelta;
		}
	}
}
function OnCollisionEnter2D(other:Collision2D)
{
	if(other.transform.gameObject.tag=="bullet")
	{
		Debug.Log("Hit");
		Debug.Log("Health = " + enemyHealth);
	}
}