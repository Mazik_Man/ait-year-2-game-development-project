﻿#pragma strict

var onSprite : Sprite;
var offSprite : Sprite;

enum lightStates 
{
	OFF,
	ON,
	MID
}

var state : lightStates = lightStates.OFF;

function Start () 
{
 	if(state == lightStates.OFF)
	{	
		GetComponent(SpriteRenderer).sprite = offSprite;	
	}
	else
	{
		GetComponent(SpriteRenderer).sprite = onSprite;	
	}
}

function Update () 
{
	// Is there a transition between states?
	if (Input.GetKey(KeyCode.UpArrow))
    {
        state = lightStates.ON;
        GetComponent(SpriteRenderer).sprite = onSprite;	
    }
    
    if (Input.GetKey(KeyCode.DownArrow))
    {
        state = lightStates.OFF;
        GetComponent(SpriteRenderer).sprite = offSprite;	
    }
}